import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import VisualitzarStream from '../views/VisualitzarStream.vue'
import Register from '../views/Register.vue'
import MovieList from '../components/MovieList.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/watch/:id',
      name: 'watch',
      component: VisualitzarStream
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/movies',
      name: 'movies',
      component: MovieList
    },
  ]
})

export default router
