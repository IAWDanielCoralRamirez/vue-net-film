import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import {createPinia} from 'pinia'
import { authStore } from './store/authStore'
import './assets/css/bootstrap.min.css'
import './assets/css/headerNavFooter.css'

const app = createApp(App)
app.use(createPinia())
app.use(router)

app.config.globalProperties.$auth = authStore();
app.mount('#app')
