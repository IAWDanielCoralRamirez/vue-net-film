import { defineStore } from 'pinia'
export const authStore = defineStore('auth', {
    state: () => {
        const auth = localStorage.getItem('auth');
        if (auth)
            return JSON.parse(auth);
      
        return {
            isAuthenticated: false,
            email: null,
            access_token: null
        }
    },
    actions: {
        async login(email,password) {

            //console.log(email, password);
           
            const response = await fetch('https://netstreamfilm.herokuapp.com/api/login/',
            {
                method:'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({email,password})
            });

            
            const data = await response.json();
            if(data.access_token) {
                this.email = email;
                this.access_token = data.access_token;
                this.isAuthenticated = true;

                localStorage.setItem('auth', JSON.stringify({ isAuthenticated: this.isAuthenticated, access_token: this.access_token, email: this.email }));
                
            }else {
                this.logout();
            }
        },
        logout(){
            this.isAuthenticated = false;
            this.email = null;
            this.access_token = null;

            localStorage.removeItem('auth');
        }
        
    }
})